package com.epam.extlab2018.finalProject.option13.service.impl;

import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.repository.RequestRepository;
import com.epam.extlab2018.finalProject.option13.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestServiceImpl implements RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public void deleteRequestById(long id) {
        requestRepository.deleteRequestById(id);
    }

    @Override
    public void updateRequestBill(long requestId, long billId) {
        requestRepository.updateRequestBill(requestId, billId);
    }

    @Override
    public void updateRequestHotelRoom(long requestId, long hotelRoomId) {
        requestRepository.updateRequestHotelRoom(requestId, hotelRoomId);
    }

    @Override
    public Request findRequestById(long requestId) {
        return requestRepository.findRequestById(requestId);
    }

    @Override
    public boolean isDateMatch(long hotelRoomId, Request request) {
        return requestRepository.isRequestDateMatch(hotelRoomId, request);
    }

    @Override
    public long createRequest(Request request) {
        return requestRepository.createRequest(request);
    }

    @Override
    public List<Request> getAllCurrentUserRequests(long userId) {
        return requestRepository.getAllCurrentUserRequests(userId);
    }

    @Override
    public List<Request> getAllAdminRequests() {
        return requestRepository.getAllAdminRequests();
    }

}
