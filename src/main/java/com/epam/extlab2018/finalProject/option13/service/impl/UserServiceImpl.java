package com.epam.extlab2018.finalProject.option13.service.impl;

import com.epam.extlab2018.finalProject.option13.dto.User;
import com.epam.extlab2018.finalProject.option13.exception.UserAlreadyExistException;
import com.epam.extlab2018.finalProject.option13.repository.UserRepository;
import com.epam.extlab2018.finalProject.option13.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public long createUser(User user) throws UserAlreadyExistException {
        try {
            findUserByLogin(user.getLogin());
            throw new UserAlreadyExistException("Пользователь с Логином " + user.getLogin() + " уже существует.");
        } catch (DataAccessException e) {
            return userRepository.createUser(user);
        }
    }

    @Override
    public User findUserByLogin(String login) {
        return userRepository.findUserByLogin(login);
    }

    @Override
    public boolean authenticateUser(String login) {
        try {
            userRepository.findUserByLogin(login);
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    @Override
    public User findUserById(long userId) {
        return userRepository.findUserById(userId);
    }

    @Override
    public void updateUser(User user, long id) {
        userRepository.updateUser(user, id);
    }
}
