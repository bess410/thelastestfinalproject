package com.epam.extlab2018.finalProject.option13.controller;

import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;
import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.service.BillService;
import com.epam.extlab2018.finalProject.option13.service.HotelRoomService;
import com.epam.extlab2018.finalProject.option13.service.RequestService;
import com.epam.extlab2018.finalProject.option13.service.SessionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

@Controller
public class HotelRoomController {

    @Autowired
    private HotelRoomService hotelRoomService;

    @Autowired
    private SessionUserService sessionUserService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private BillService billService;

    @GetMapping("admin/requests/{id}")
    private ModelAndView hotelRooms(@PathVariable long id, ModelAndView modelAndView) {
        modelAndView.addObject("currentUser", sessionUserService.getCurrentSessionUser());
        List<Request> requests = requestService.getAllAdminRequests();
        modelAndView.addObject("requests", requests);

        List<HotelRoom> hotelRooms = hotelRoomService.getHotelRoomsToRequest(id);
        modelAndView.addObject("rooms", hotelRooms);
        modelAndView.setViewName("admin_requests");
        return modelAndView;
    }

    @GetMapping("admin/requests/{id}/{hotelRoomId}")
    private ModelAndView hotelRoomsUpdate(@PathVariable long id, @PathVariable long hotelRoomId, ModelAndView modelAndView) {
        requestService.updateRequestHotelRoom(id, hotelRoomId);

        // Узнаем количество дней проживания в номере
        Request request = requestService.findRequestById(id);
        long days = DAYS.between(request.getStart(), request.getEnd()) + 1;

        // Узнаем стоимость номера
        HotelRoom hotelRoom = hotelRoomService.findHotelRoomById(hotelRoomId);
        BigDecimal score = hotelRoom.getDailyCost().multiply(new BigDecimal(days));

        //Создаем счет
        long billId = billService.createBill(score, false);

        //Выставляем счет клиенту
        requestService.updateRequestBill(id, billId);
        modelAndView.setViewName("redirect:..");
        return modelAndView;
    }

    @GetMapping("client/requests/hotelroom/{id}")
    private ModelAndView hotelRoomClient(@PathVariable long id, ModelAndView modelAndView) {
        modelAndView.addObject("currentUser", sessionUserService.getCurrentSessionUser());
        List<Request> requests = requestService.getAllCurrentUserRequests(sessionUserService.getCurrentSessionUser().getUserId());
        modelAndView.addObject("requests", requests);
        HotelRoom hotelRoom = hotelRoomService.findHotelRoomById(id);
        modelAndView.addObject("room", hotelRoom);
        modelAndView.setViewName("user_requests");
        return modelAndView;
    }


}
