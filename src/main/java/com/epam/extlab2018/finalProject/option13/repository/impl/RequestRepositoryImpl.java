package com.epam.extlab2018.finalProject.option13.repository.impl;

import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;
import com.epam.extlab2018.finalProject.option13.enums.ApartmentClass;
import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.repository.RequestRepository;
import com.epam.extlab2018.finalProject.option13.util.mappers.RequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;

@Repository
public class RequestRepositoryImpl implements RequestRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public long createRequest(Request request) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("seatNumber", request.getSeatNumber())
                .addValue("apartmentClass", request.getApartmentClass().toString())
                .addValue("start", request.getStart())
                .addValue("end", request.getEnd())
                .addValue("user_id", request.getUserId());
        namedParameterJdbcTemplate.update("Insert into request (seatNumber, apartmentClass, start, end, user_id) Values" +
                "(:seatNumber, :apartmentClass, :start, :end, :user_id)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void updateRequestBill(long requestId, long billId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update request Set bill_id = ? Where ID = ?",
                billId, requestId);
    }

    @Override
    public void updateRequestHotelRoom(long requestId, long hotelRoomId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update request Set hotelRoom_id = ? Where ID = ?",
                hotelRoomId, requestId);
    }


    @Override
    public boolean isRequestDateMatch(long hotelRoomId, Request request) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "SELECT count(*) from request where hotelroom_id = ? " +
                "and ((DATE(start) between ? and ?) or (DATE(end) between ? and ?))";
        int count = jdbcTemplate.queryForObject(sql, new Object[]{hotelRoomId, request.getStart(), request.getEnd(),
                request.getStart(), request.getEnd()}, Integer.class);
        return count > 0;
}

    @Override
    public void deleteRequestById(long requestId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete From request Where id = ?", requestId);
    }

    @Override
    public Request findRequestById(long requestId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From request Where id = ?", new Object[]{requestId}, new RequestMapper());
    }

    @Override
    public List<Request> getAllCurrentUserRequests(long userId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From request Where user_id = " + userId + " and Date(end) >= curdate()", new RequestMapper());
    }

    @Override
    public List<Request> getAllAdminRequests() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From request Where bill_id is null", new RequestMapper());
    }
}
