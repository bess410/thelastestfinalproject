package com.epam.extlab2018.finalProject.option13.util.mappers;

import com.epam.extlab2018.finalProject.option13.dto.Bill;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillMapper implements RowMapper<Bill> {
    @Override
    public Bill mapRow(ResultSet rs, int rowNums) throws SQLException {
        Bill bill = new Bill();
        bill.setBillId(rs.getLong("id"));
        bill.setScore(rs.getBigDecimal("score"));
        bill.setPaid(rs.getBoolean("isPaid"));
        return bill;
    }
}
