package com.epam.extlab2018.finalProject.option13.service;

import com.epam.extlab2018.finalProject.option13.dto.Bill;

import java.math.BigDecimal;

public interface BillService {
    long createBill(BigDecimal score, boolean isPaid);

    Bill findBillById(long id);

    void payBill(long id);

    void deleteBillById(long billId);
}
