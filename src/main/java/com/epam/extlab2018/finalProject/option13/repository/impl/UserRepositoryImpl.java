package com.epam.extlab2018.finalProject.option13.repository.impl;

import com.epam.extlab2018.finalProject.option13.enums.Role;
import com.epam.extlab2018.finalProject.option13.dto.User;
import com.epam.extlab2018.finalProject.option13.exception.UserAlreadyExistException;
import com.epam.extlab2018.finalProject.option13.repository.UserRepository;
import com.epam.extlab2018.finalProject.option13.util.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public User findUserByLogin(String login) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From user Where login = ?", new Object[]{login}, new UserMapper());
    }

    @Override
    public long createUser(User user) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", user.getName())
                .addValue("surname", user.getSurname())
                .addValue("login", user.getLogin())
                .addValue("pass", user.getPass())
                .addValue("role", user.getRole().toString());
        namedParameterJdbcTemplate.update("Insert into user (name, surname, login, pass, role) Values" +
                "(:name, :surname, :login, :pass, :role)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public User findUserById(long userId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From user Where id = ?", new Object[]{userId}, new UserMapper());
    }

    @Override
    public void updateUser(User user, long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update user Set name = ?, surname = ?, login = ?, pass = ? Where ID = ?",
                user.getName(), user.getSurname(), user.getLogin(), user.getPass(), id);
    }
}
