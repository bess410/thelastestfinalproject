package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.Request;

import java.util.List;

public interface RequestRepository {
    long createRequest(Request request);

    void updateRequestHotelRoom(long requestId, long hotelRoomId);

    void deleteRequestById(long requestId);

    Request findRequestById(long requestId);

    List<Request> getAllCurrentUserRequests(long userId);

    List<Request> getAllAdminRequests();

    void updateRequestBill(long requestId, long billId);

    boolean isRequestDateMatch(long hotelRoomId, Request request);
}
