package com.epam.extlab2018.finalProject.option13.repository.impl;

import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.enums.ApartmentClass;
import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;
import com.epam.extlab2018.finalProject.option13.repository.HotelRoomRepository;
import com.epam.extlab2018.finalProject.option13.util.mappers.HotelRoomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.List;

@Repository
public class HotelRoomRepositoryImpl implements HotelRoomRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public HotelRoom findHotelRoomById(long hotelRoomId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From hotelRoom Where id = ?", new Object[]{hotelRoomId}, new HotelRoomMapper());
    }

    @Override
    public List<HotelRoom> getAllNeededRooms(Request request) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sqlAllNeededRooms = "SELECT * from hotelroom where seatNumber = ? and apartmentClass = ?";
        return jdbcTemplate.query(sqlAllNeededRooms, new Object[]{request.getSeatNumber(), request.getApartmentClass().toString()}, new HotelRoomMapper());
    }
}
