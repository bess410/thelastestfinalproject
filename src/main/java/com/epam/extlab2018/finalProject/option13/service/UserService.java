package com.epam.extlab2018.finalProject.option13.service;

import com.epam.extlab2018.finalProject.option13.dto.User;
import com.epam.extlab2018.finalProject.option13.exception.UserAlreadyExistException;

public interface UserService {
    long createUser(User user) throws UserAlreadyExistException;

    boolean authenticateUser(String login);

    User findUserById(long userId);

    User findUserByLogin(String login);

    void updateUser(User user, long id);
}
