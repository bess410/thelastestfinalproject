package com.epam.extlab2018.finalProject.option13.util.mappers;

import com.epam.extlab2018.finalProject.option13.enums.ApartmentClass;
import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class HotelRoomMapper implements RowMapper<HotelRoom> {
    @Override
    public HotelRoom mapRow(ResultSet rs, int rowNums) throws SQLException {
        HotelRoom hotelRoom = new HotelRoom();
        hotelRoom.setHotelRoomId(rs.getLong("id"));
        hotelRoom.setSeatNumber(rs.getInt("seatNumber"));
        hotelRoom.setApartmentClass(ApartmentClass.valueOf(rs.getString("apartmentClass").toUpperCase()));
        hotelRoom.setDailyCost(rs.getBigDecimal("dailyCost"));
        hotelRoom.setAddress(rs.getString("address"));
        return hotelRoom;
    }
}
