package com.epam.extlab2018.finalProject.option13.interceptor;

import com.epam.extlab2018.finalProject.option13.dto.User;
import com.epam.extlab2018.finalProject.option13.enums.Role;
import com.epam.extlab2018.finalProject.option13.service.SessionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdminInterceptor implements HandlerInterceptor {

    @Autowired
    private SessionUserService sessionUserService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        User user = sessionUserService.getCurrentSessionUser();

        if (user.getRole() == Role.CLIENT) {
            response.sendRedirect("/client");
            return false;
        }

        return true;
    }
}