package com.epam.extlab2018.finalProject.option13.controller;

import com.epam.extlab2018.finalProject.option13.dto.Bill;
import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.service.BillService;
import com.epam.extlab2018.finalProject.option13.service.RequestService;
import com.epam.extlab2018.finalProject.option13.service.SessionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class RequestController {

    @Autowired
    private SessionUserService sessionUserService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private BillService billService;

    @GetMapping("client/requests")
    private ModelAndView clientRequestsGet(ModelAndView modelAndView) {
        modelAndView.addObject("currentUser", sessionUserService.getCurrentSessionUser());
        List<Request> requests = requestService.getAllCurrentUserRequests(sessionUserService.getCurrentSessionUser().getUserId());
        modelAndView.addObject("requests", requests);
        modelAndView.setViewName("user_requests");
        return modelAndView;
    }

    @PostMapping("client/requests")
    private ModelAndView clientRequestsPost(ModelAndView modelAndView, HttpServletRequest request, Request requestHotel) {
        requestHotel.setUserId(sessionUserService.getCurrentSessionUser().getUserId());
        requestService.createRequest(requestHotel);
        modelAndView.setViewName("redirect:../client/requests");
        return modelAndView;
    }

    @GetMapping("client/requests/{id}/delete")
    private ModelAndView clientRequestDelete(@PathVariable long id, ModelAndView modelAndView) {
        Request request = requestService.findRequestById(id);
        // Если счет выставлен, проверяем оплачен ли он
        if (request.getBillId() > 0) {
            Bill bill = billService.findBillById(request.getBillId());
            // Если счет еще не оплачен, то удаляем заявку и выставленный счет
            if (!bill.getIsPaid()) {
                requestService.deleteRequestById(id);
                billService.deleteBillById(bill.getBillId());
            }
        } else {
            requestService.deleteRequestById(id);
        }

        modelAndView.setViewName("redirect:/client/requests");
        return modelAndView;
    }

    @GetMapping("admin/requests")
    private ModelAndView adminRequestsGet(ModelAndView modelAndView) {
        modelAndView.addObject("currentUser", sessionUserService.getCurrentSessionUser());
        List<Request> requests = requestService.getAllAdminRequests();
        modelAndView.addObject("requests", requests);
        modelAndView.setViewName("admin_requests");
        return modelAndView;
    }

}
