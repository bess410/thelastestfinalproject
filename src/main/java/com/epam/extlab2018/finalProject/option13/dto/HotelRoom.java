package com.epam.extlab2018.finalProject.option13.dto;

import com.epam.extlab2018.finalProject.option13.enums.ApartmentClass;

import java.math.BigDecimal;

public class HotelRoom {
    private long hotelRoomId;
    private int seatNumber;
    private String address;


    private ApartmentClass apartmentClass;
    private BigDecimal dailyCost;

    public HotelRoom() {
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getHotelRoomId() {
        return hotelRoomId;
    }

    public void setHotelRoomId(long hotelRoomId) {
        this.hotelRoomId = hotelRoomId;
    }

    public BigDecimal getDailyCost() {
        return dailyCost;
    }

    public void setDailyCost(BigDecimal dailyCost) {
        this.dailyCost = dailyCost;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public ApartmentClass getApartmentClass() {
        return apartmentClass;
    }

    public void setApartmentClass(ApartmentClass apartmentClass) {
        this.apartmentClass = apartmentClass;
    }

    @Override
    public String toString() {
        return "HotelRoom{" +
                "hotelRoomId=" + hotelRoomId +
                ", seatNumber=" + seatNumber +
                ", address='" + address + '\'' +
                ", apartmentClass=" + apartmentClass +
                ", dailyCost=" + dailyCost +
                '}';
    }
}
