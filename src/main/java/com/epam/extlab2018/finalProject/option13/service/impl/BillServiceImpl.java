package com.epam.extlab2018.finalProject.option13.service.impl;

import com.epam.extlab2018.finalProject.option13.dto.Bill;
import com.epam.extlab2018.finalProject.option13.repository.BillRepository;
import com.epam.extlab2018.finalProject.option13.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class BillServiceImpl implements BillService {

    @Autowired
    private BillRepository billRepository;

    @Override
    public void deleteBillById(long billId) {
        billRepository.deleteBill(billId);
    }

    @Override
    public void payBill(long id) {
        billRepository.payBill(id);
    }

    @Override
    public Bill findBillById(long id) {
        if (id == 0) {
            return null;
        }
        return billRepository.findBillById(id);
    }

    @Override
    public long createBill(BigDecimal score, boolean isPaid) {
        return billRepository.createBill(score, isPaid);
    }
}
