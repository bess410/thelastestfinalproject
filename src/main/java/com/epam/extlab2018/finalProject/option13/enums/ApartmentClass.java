package com.epam.extlab2018.finalProject.option13.enums;

public enum ApartmentClass {
    ECONOMIC("Эконом"), BUSINESS("Бизнес"), LUX("Люкс");
    private String name;

    public String getName() {
        return name;
    }

    ApartmentClass(String name) {
        this.name = name;
    }
}
