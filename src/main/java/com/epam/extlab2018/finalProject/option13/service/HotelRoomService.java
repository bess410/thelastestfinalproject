package com.epam.extlab2018.finalProject.option13.service;

import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;

import java.util.List;

public interface HotelRoomService {
    List<HotelRoom> getHotelRoomsToRequest(long requestId);

    HotelRoom findHotelRoomById(long hotelRoomId);
}
