package com.epam.extlab2018.finalProject.option13.service.impl;

import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;
import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.repository.HotelRoomRepository;
import com.epam.extlab2018.finalProject.option13.service.HotelRoomService;
import com.epam.extlab2018.finalProject.option13.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotelRoomServiceImpl implements HotelRoomService {

    @Autowired
    private HotelRoomRepository hotelRoomRepository;

    @Autowired
    private RequestService requestService;

    @Override
    public List<HotelRoom> getHotelRoomsToRequest(long requestId) {
        Request request = requestService.findRequestById(requestId);

        // Получаем все номера по seatNumber и apartmentClass
        List<HotelRoom> resultAllRooms = hotelRoomRepository.getAllNeededRooms(request);


        // Проверяем есть ли id номера в таблице request
        return resultAllRooms.stream().filter(room -> (!requestService.isDateMatch(room.getHotelRoomId(), request)))
                .collect(Collectors.toList());
    }

    @Override
    public HotelRoom findHotelRoomById(long hotelRoomId) {
        if (hotelRoomId == 0) {
            return null;
        }
        return hotelRoomRepository.findHotelRoomById(hotelRoomId);
    }


}
