package com.epam.extlab2018.finalProject.option13.controller;

import com.epam.extlab2018.finalProject.option13.dto.Bill;
import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.service.BillService;
import com.epam.extlab2018.finalProject.option13.service.RequestService;
import com.epam.extlab2018.finalProject.option13.service.SessionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("client/requests/bill")
public class BillController {

    @Autowired
    private SessionUserService sessionUserService;

    @Autowired
    private BillService billService;

    @Autowired
    private RequestService requestService;

    @GetMapping("{id}")
    private ModelAndView requestBill(@PathVariable long id, ModelAndView modelAndView) {
        modelAndView.addObject("currentUser", sessionUserService.getCurrentSessionUser());
        List<Request> requests = requestService.getAllCurrentUserRequests(sessionUserService.getCurrentSessionUser().getUserId());
        modelAndView.addObject("requests", requests);

        Bill bill = billService.findBillById(id);
        modelAndView.addObject("bill", bill);
        modelAndView.setViewName("user_requests");
        return modelAndView;
    }

    @GetMapping("{id}/pay")
    private ModelAndView hotelRooms(@PathVariable long id, ModelAndView modelAndView) {
        billService.payBill(id);
        modelAndView.setViewName("redirect:/client/requests/bill/" + id);
        return modelAndView;
    }

}
