package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;

import java.util.List;

public interface HotelRoomRepository {
    HotelRoom findHotelRoomById(long hotelRoomId);

    List<HotelRoom> getAllNeededRooms(Request request);
}
