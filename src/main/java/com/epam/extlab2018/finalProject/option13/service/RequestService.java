package com.epam.extlab2018.finalProject.option13.service;

import com.epam.extlab2018.finalProject.option13.dto.Request;

import java.util.List;

public interface RequestService {

    long createRequest(Request request);

    List<Request> getAllCurrentUserRequests(long userId);

    List<Request> getAllAdminRequests();

    Request findRequestById(long requestId);

    void updateRequestHotelRoom(long requestId, long hotelRoomId);

    void updateRequestBill(long requestId, long billId);

    void deleteRequestById(long id);

    boolean isDateMatch(long hotelRoomId, Request request);
}
