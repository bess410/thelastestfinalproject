package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.User;

public interface UserRepository {
    long createUser(User user);

    User findUserById(long userId);

    User findUserByLogin(String login);

    void updateUser(User user, long id);
}
