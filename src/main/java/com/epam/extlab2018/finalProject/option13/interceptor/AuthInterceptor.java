package com.epam.extlab2018.finalProject.option13.interceptor;

import com.epam.extlab2018.finalProject.option13.dto.User;
import com.epam.extlab2018.finalProject.option13.service.SessionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    private SessionUserService sessionUserService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        User user = sessionUserService.getCurrentSessionUser();

        if (Objects.isNull(user)) {
            response.sendRedirect("/login");
            return false;
        }

        return true;
    }
}