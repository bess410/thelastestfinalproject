package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.Bill;

import java.math.BigDecimal;

public interface BillRepository {
    long createBill(BigDecimal score, boolean isPaid);

    void deleteBill(long billId);

    Bill findBillById(long billId);

    void payBill(long id);
}
