package com.epam.extlab2018.finalProject.option13.controller;

import com.epam.extlab2018.finalProject.option13.dto.User;
import com.epam.extlab2018.finalProject.option13.exception.UserAlreadyExistException;
import com.epam.extlab2018.finalProject.option13.service.SessionUserService;
import com.epam.extlab2018.finalProject.option13.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AuthController {

    @Autowired
    private SessionUserService sessionUserService;

    @Autowired
    private UserService userService;

    @GetMapping("registration")
    public ModelAndView registration(ModelAndView modelAndView) {
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @PostMapping("registration")
    public ModelAndView registration(ModelAndView modelAndView, HttpServletRequest request, User user) {

        try {
            long userId = userService.createUser(user);
            User userFromDb = userService.findUserById(userId);
            sessionUserService.setCurrentSessionUser(userFromDb);
        } catch (UserAlreadyExistException e) {
            modelAndView.addObject("error", e.getMessage());
            modelAndView.addObject("user", user);
            modelAndView.setViewName("registration");
            return modelAndView;
        }

        modelAndView.setViewName("redirect:client/requests");
        return modelAndView;
    }


    @GetMapping("logout")
    public ModelAndView logout(ModelAndView modelAndView, HttpServletRequest request) {
        request.getSession().invalidate();
        modelAndView.setViewName("redirect:");
        return modelAndView;
    }

    @GetMapping("login")
    public ModelAndView login(ModelAndView modelAndView) {
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @PostMapping("login")
    public ModelAndView login(ModelAndView modelAndView, HttpServletRequest request, User user) {
        boolean authenticated = userService.authenticateUser(user.getLogin());

        if (authenticated) {
            User userFromDb = userService.findUserByLogin(user.getLogin());
            // Проверяем пароль
            if (userFromDb.getPass().equals(user.getPass())) {
                sessionUserService.setCurrentSessionUser(userFromDb);
                modelAndView.setViewName("redirect:client/requests");
            } else {
                modelAndView.addObject("error", "Неверный логин или пароль");
                modelAndView.addObject("user", user);
                modelAndView.setViewName("login");
            }
        } else {
            modelAndView.addObject("error", "Неверный логин или пароль");
            modelAndView.addObject("user", user);
            modelAndView.setViewName("login");
        }
        return modelAndView;
    }
}