package com.epam.extlab2018.finalProject.option13.controller;

import com.epam.extlab2018.finalProject.option13.service.SessionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class IndexController implements ErrorController {

    @Autowired
    private SessionUserService sessionUserService;

    private static final String PATH = "/error";

    @GetMapping(PATH)
    private ModelAndView clientSettingsPost(ModelAndView modelAndView) {
        modelAndView.addObject("currentUser", sessionUserService.getCurrentSessionUser());
        modelAndView.setViewName("error-page");
        return modelAndView;
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}