package com.epam.extlab2018.finalProject.option13.service;

import com.epam.extlab2018.finalProject.option13.dto.User;

public interface SessionUserService {
    User setCurrentSessionUser(User user);

    User getCurrentSessionUser();
}
