package com.epam.extlab2018.finalProject.option13.controller;

import com.epam.extlab2018.finalProject.option13.dto.User;
import com.epam.extlab2018.finalProject.option13.service.SessionUserService;
import com.epam.extlab2018.finalProject.option13.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClientController {

    @Autowired
    private SessionUserService sessionUserService;

    @Autowired
    private UserService userService;

    @GetMapping("client")
    private ModelAndView client(ModelAndView modelAndView) {
        modelAndView.setViewName("redirect:client/requests");
        return modelAndView;
    }

    @GetMapping("client/settings")
    private ModelAndView clientSettings(ModelAndView modelAndView) {
        modelAndView.addObject("currentUser", sessionUserService.getCurrentSessionUser());
        modelAndView.setViewName("user_settings");
        return modelAndView;
    }

    @PostMapping("client/settings")
    private ModelAndView clientSettingsPost(ModelAndView modelAndView, User user) {
        long userId = sessionUserService.getCurrentSessionUser().getUserId();
        userService.updateUser(user, userId);
        sessionUserService.setCurrentSessionUser(userService.findUserById(userId));
        modelAndView.addObject("currentUser", sessionUserService.getCurrentSessionUser());
        modelAndView.setViewName("user_settings");
        return modelAndView;
    }
}
