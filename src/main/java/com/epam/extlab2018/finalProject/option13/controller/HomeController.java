package com.epam.extlab2018.finalProject.option13.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @GetMapping("/")
    private ModelAndView homeGet(ModelAndView modelAndView) {
        modelAndView.setViewName("redirect:login");
        return modelAndView;
    }

    @PostMapping("/")
    private ModelAndView homePost(ModelAndView modelAndView) {
        modelAndView.setViewName("redirect:login");
        return modelAndView;
    }
}
