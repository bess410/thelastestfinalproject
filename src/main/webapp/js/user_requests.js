$(document).ready(function(){
    var location = window.location.href;
    if((location.split('/').pop() >= 0) && (location.indexOf("bill") > 0)){
        $("#overlay, #popup_bill").show();
    }

    if((location.split('/').pop() >= 0) && (location.indexOf("hotelroom") > 0)){
        $("#overlay, #popup_room").show();
    }

	$("#create_request").on("click", function(){
		$("#overlay, #popup_create_request").show();
		return false;
	});
	
	$(".modal_close, #overlay").click( function(){
		$("#popup_change_request, #popup_bill, #popup_room, #popup_create_request, #overlay").hide();
		return false;
	});
	
	$("#create_new_request").on("click", function(){
	    var start = $("#start_new").val();
        var end = $("#end_new").val();
        var isValidate = true;

        if(start == ""){
            $(".warning.start_new span").text("Поле не должно быть пустым");
            $(".warning.start_new span").show();
            isValidate = false;
        } else {
            $(".warning.start_new span").hide();
            start = new Date(start);
        }

        if(end == ""){
            $(".warning.end_new span").text("Поле не должно быть пустым");
            $(".warning.end_new span").show();
            isValidate = false;
        } else {
            $(".warning.end_new span").hide();
            end = new Date(end);
        }

        if(start < new Date()){
            $(".warning.start_new span").text("Дата заселения должна быть больше текущей даты");
            $(".warning.start_new span").show();
            isValidate = false;
        }

        if(start > end){
            $(".warning.end_new span").text("Дата выезда должна быть больше или равна дате заселения");
            $(".warning.end_new span").show();
            isValidate = false;
        }

        if(isValidate){
            $("#popup_create_request, #overlay").hide();
        }
		return isValidate;
	});
});


