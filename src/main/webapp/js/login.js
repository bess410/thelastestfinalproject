$(document).ready(function(){
    $("input").on('keyup',function(){
    		var id;
    		if($(this).val() == ""){
    			id = $(this).attr("id");
    			$(".warn." + id +" span").show();
    		} else {
    			id = $(this).attr("id");
    			$(".warn." + id +" span").hide();
    		}
    	});


	$("#enter").click(function(){
		var login = $("#login").val();
		var pass = $("#pass").val();

		var isValidate = true;


        $("#login").val(login);
        $("#pass").val(pass);
        if(login == ""){
            $(".warn.login span").text("Поле Логин не должно быть пустым");
            $(".warn.login span").show();
            isValidate = false;
        }

        if(pass == ""){
            $(".warn.pass span").text("Поле Пароль не должно быть пустым");
            $(".warn.pass span").show();
            isValidate = false;
        }

        return isValidate;
    });
});
