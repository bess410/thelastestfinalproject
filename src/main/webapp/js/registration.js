$(document).ready(function(){
	$("input").on('keyup',function(){
		var id;
		if($(this).val() == ""){
			id = $(this).attr("id");
			$(".warning." + id +" span").show();
		} else {
			id = $(this).attr("id");
			$(".warning." + id +" span").hide();
		}
	});
	
	$("#btn_reg").on("click", function(){
		var name = $("#name").val();
		var surname = $("#surname").val();
		var login = $("#login").val();
		var pass = $("#pass").val();
		var isValidate = true;
		if(name == ""){
			$(".warning.name span").show();
			isValidate = false;
		}
		if(surname == ""){
			$(".warning.surname span").show();
			isValidate = false;
		}
		if(login == ""){
			$(".warning.login span").show();
			isValidate = false;
		}
		if(pass == ""){
			$(".warning.pass span").show();
			isValidate = false;
		}
		return isValidate;
	});
});