<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%  pageContext.setAttribute("apartmentClass", com.epam.extlab2018.finalProject.option13.enums.ApartmentClass.values()); %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link href="/css/style.css" rel="stylesheet" />
		<title>Мои заявки</title>
		<link rel="shortcut icon" href="/css/img/hotel.ico" type="image/x-icon" />
		<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
		<script src="/js/jquery-3.3.1.js"></script>
		<script src="/js/user_requests.js"></script>
		<script src="/js/menu.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
			</div>
			<div id="content">
				<div id="left_panel">
					<div class="header">
						Здравствуйте, ${currentUser.name}
					</div>
					<ul class="user_menu">
						<li><a href="/client/requests">Мои заявки</a></li>
						<li><a href="/client/settings">Настройки</a></li>
						<li><a href="/logout">Выход</a></li>
					</ul>
				</div>
				<div id="right_panel">
					<a id="create_request" href="">Создать новую заявку</a>
					<table id="requests">
						<tr>
							<th>Номер заявки</th>
							<th>Количество проживающих</th>
							<th>Класс</th>
							<th>Начало проживания</th>
							<th>Окончание проживания</th>
							<th>Счет</th>
							<th>Номер</th>
						</tr>
						<c:forEach var="request" items="${requests}">
                            <tr>
                                <td>${request.requestId}</td>
                                <td>${request.seatNumber}</td>
                                <td>${request.apartmentClass.name}</td>
                                <td><javatime:format value="${request.start}" /></td>
                                <td><javatime:format value="${request.end}" /></td>
                                <td><a class="popup_bill" href="/client/requests/bill/${request.billId}">Просмотр</a></td>
                                <td><a class="popup_room" href="/client/requests/hotelroom/${request.hotelRoomId}">Просмотр</a></td>
                            </tr>
                            <tr>
                                <td colspan="7">
                                    <a class="delete" href="/client/requests/${request.requestId}/delete">Удалить</a>
                                </td>
                            </tr>
                        </c:forEach>
					</table>
				</div>
			</div>
			<div id="footer">
				<div id="contacts">
					<p>Адрес:
					г. Ижевск ул. Ленина д.1 офис 35</p>
					<p>Телефон:
					+7(3412)76-33-59</p>
					<p>Skype:
					hotroomsizhevsk</p>
				</div>
			</div>
		</div>
		<div id="popup_bill">
			<span class="modal_close">X</span>
			<table>
				<tr>
					<th>Номер счета</th>
					<th>Сумма к оплате</th>
					<th>Статус</th>
				</tr>
				<c:if test="${empty bill}">
                    <tr><td colspan="3">Вам еще не выставлен счет</td></tr>
                </c:if>
                <c:if test="${not empty bill}">
                    <tr>
                        <td>${bill.billId}</td>
                        <td>${bill.score}</td>
                        <td>${!bill.isPaid ? "Не оплачен" : "Счет оплачен"}</td>
                    </tr>
                    <c:if test="${!bill.isPaid}">
                        <tr>
                            <td colspan="5">
                                <a id="pay_bill" href="/client/requests/bill/${bill.billId}/pay">Оплатить</a>
                            </td>
                        </tr>
                    </c:if>
                </c:if>
			</table>
		</div>
		<div id="popup_room">
			<span class="modal_close">X</span>
			<table>
				<tr>
					<th>Номер заявки</th>
					<th>Адрес гостиницы</th>
					<th>Количество мест</th>
					<th>Класс номера</th>
					<th>Стоимость (сутки)</th>
				</tr>
				<c:if test="${empty room}">
                    <tr><td colspan="5">Вам еще не подобран гостиничный номер.</td></tr>
                </c:if>
                <c:if test="${not empty room}">
                    <tr>
                        <td>${room.hotelRoomId}</td>
                        <td>${room.address}</td>
                        <td>${room.seatNumber}</td>
                        <td>${room.apartmentClass.name}</td>
                        <td>${room.dailyCost}</td>
                    </tr>
                </c:if>
			</table>
		</div>
		<div id="popup_create_request">
			<span class="modal_close">X</span>
			<form id="request_form_new" action="/client/requests" method="post">
				<div class="left">
					<div>
						<label for="seatNumber_new">Количество проживающих:</label><br/>
						<select id="seatNumber_new" name="seatNumber">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</div>
					<div class="top">
                        <label for="start_new">Начало проживания:</label><br/>
                        <input id="start_new" name="start" type="date"/>
                        <p class="warning start_new"><span></span></p>
                    </div>
				</div>
				<div class="right">
					<div>
                        <label for="apartmentClass_new">Класс номера:</label><br/>
                        <select id="apartmentClass_new" name="apartmentClass">
                            <c:forEach var="entry" items="${apartmentClass}">
                                <option value=${entry}>${entry.name}</option>
                            </c:forEach>
                        </select>
                    </div>
					<div class="top">
						<label for="end_new">Окончание проживания:</label><br/>
						<input id="end_new" name="end" type="date"/>
						<p class="warning end_new"><span></span></p>
					</div>
				</div>
				<input id="create_new_request" type="submit" name="submit" value="Создать заявку"/>
			</form>
		</div>
		<div id="overlay"></div>
	</body>
</html>