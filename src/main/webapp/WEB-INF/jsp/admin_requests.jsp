<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link href="/css/style.css" rel="stylesheet" />
		<link rel="shortcut icon" href="/css/img/hotel.ico" type="image/x-icon" />
		<title>Новые заявки</title>
		<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
		<script src="/js/jquery-3.3.1.js"></script>
		<script src="/js/admin_requests.js"></script>
		<script src="/js/menu.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
			</div>
			<div id="content">
				<div id="left_panel">
					<div class="header">
						Администратор: ${currentUser.name}
					</div>
					<ul class="user_menu">
                        <li><a href="/admin/requests">Новые заявки</a></li>
                        <li><a href="/admin/settings">Настройки</a></li>
                        <li><a href="/logout">Выход</a></li>
                    </ul>
				</div>
				<div id="right_panel">
					<table class="clients">
						<tr>
							<th>Id клиента</th>
							<th>Номер заявки</th>
							<th>Количество проживающих</th>
							<th>Класс</th>
							<th>Начало проживания</th>
							<th>Окончание проживания</th>
							<th>Id номера</th>
						</tr>
						<c:forEach var="request" items="${requests}">
                            <tr>
                                <td>${request.userId}</td>
                                <td>${request.requestId}</td>
                                <td>${request.seatNumber}</td>
                                <td>${request.apartmentClass.name}</td>
                                <td><javatime:format value="${request.start}" /></td>
                                <td><javatime:format value="${request.end}" /></td>
                                <td>
                                    <a class="get_room" href="/admin/requests/${request.requestId}">
                                        ${request.hotelRoomId == 0 ? "Подобрать номер" : request.hotelRoomId}
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
					</table>
				</div>
			</div>
			<div id="footer">
				<div id="contacts">
					<p>Адрес:
					г. Ижевск ул. Ленина д.1 офис 35</p>
					<p>Телефон:
					+7(3412)76-33-59</p>
					<p>Skype:
					hotroomsizhevsk</p>
				</div>
			</div>
			<div id="popup_get_room">
				<span class="modal_close">X</span>
				<table>
					<tr>
						<th>Id номера</th>
						<th>Адрес гостиницы</th>
						<th>Количество мест</th>
						<th>Класс номера</th>
						<th>Стоимость (сутки)</th>
						<th>Выбрать номер</th>
					</tr>
					<c:if test="${empty rooms}">
                        <tr><td colspan="6">Нет доступных номеров</td></tr>
                    </c:if>
                    <c:if test="${not empty rooms}">
                        <c:forEach var="room" items="${rooms}">
                            <tr>
                                <td>${room.hotelRoomId}</td>
                                <td>${room.address}</td>
                                <td>${room.seatNumber}</td>
                                <td>${room.apartmentClass.name}</td>
                                <td>${room.dailyCost}</td>
                                <td><a class="choose_room" href="${room.hotelRoomId}">Выбрать</a></td>
                            </tr>
                        </c:forEach>
                    </c:if>
				</table>
			</div>
			<div id="overlay"></div>
		</div>
	</body>
</html>