<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link href="/css/style.css" rel="stylesheet" />
		<title>Мои заявки</title>
		<link rel="shortcut icon" href="/css/img/hotel.ico" type="image/x-icon" />
		<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
		<script src="/js/jquery-3.3.1.js"></script>
		<script src="/js/user_requests.js"></script>
		<script src="/js/menu.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
			</div>
			<div id="content">
				<div id="left_panel">
					<div class="header">
						Здравствуйте, ${currentUser.name}
					</div>
					<ul class="user_menu">
                        <li><a href="/client/requests">Мои заявки</a></li>
                        <li><a href="/client/settings">Настройки</a></li>
                        <li><a href="/logout">Выход</a></li>
                    </ul>
				</div>
				<div id="right_panel">
                    <h1>Страница, на которую вы пытаетесь попасть, не существует. Либо произошла ошибка на сервере.<br/>
                        Мы уже решаем данную проблему. Заходите позже.</h1>
				</div>
			</div>
			<div id="footer">
				<div id="contacts">
					<p>Адрес:
					г. Ижевск ул. Ленина д.1 офис 35</p>
					<p>Телефон:
					+7(3412)76-33-59</p>
					<p>Skype:
					hotroomsizhevsk</p>
				</div>
			</div>
		</div>
	</body>
</html>