<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link href="/css/style.css" rel="stylesheet" />
		<link rel="shortcut icon" href="/css/img/hotel.ico" type="image/x-icon" />
		<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
		<script src="/js/jquery-3.3.1.js"></script>
		<script src="/js/login.js"></script>
		<title>Форма авторизации</title>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
			</div>
			<div id="content">
				<form id="login_form" action="login" method="post">
					<div class="left">
						<label for="login">Логин:</label><br/>
						<input id="login" name="login" type="text" value="${user.login}"/>
						<p class="warn login"><span>Поле Логин не должно быть пустым</span></p>
					</div>
					<div class="top left">
						<label for="pass">Пароль:</label><br/>
						<input id="pass" name="pass" type="password" value="${user.pass}"/>
						<p class="warn pass"><span>Поле Пароль не должно быть пустым</span></p>
					</div>
					<a class="left top" href="registration">Регистрация</a>
					<input id="enter" class="right top" type="submit" value="Вход"/>
				</form>
				<p class="error"><span>${error}</span></p>
			</div>
			<div id="footer">
				<div id="contacts">
					<p>Адрес:
					г. Ижевск ул. Ленина д.1 офис 35</p>
					<p>Телефон:
					+7(3412)76-33-59</p>
					<p>Skype:
					hotroomsizhevsk</p>
				</div>
			</div>
		</div>
	</body>
</html>